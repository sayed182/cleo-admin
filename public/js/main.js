(function($){
    
// Modal Controller
var modalClickElement = $('[data-toggle=modal]');
var closeButton = $('.modal #close-button');
var modalStage = $('.modal');
modalClickElement.on('click', function(event){
    var target = $( '#'+$(this).data('target-modal'));
    console.log(target);
    target.removeClass('fade');
})
closeButton.on('click', function(){
    modalStage.addClass('fade');
});


// Activation  of DataTables
try {
    $("#admin-overview-table").DataTable();
    $('#student-transaction-table').DataTable();
}catch (e) {

}



$('.browse-button').on('click', function(){
    $('#document-upload').trigger('click');
});


}(jQuery));
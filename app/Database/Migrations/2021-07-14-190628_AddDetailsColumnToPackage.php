<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddDetailsColumnToPackage extends Migration
{
	public function up()
	{
        $fields = [
            'details' => ['type' => 'TEXT', 'null' => true]
        ];
        $this->forge->addColumn('package', $fields);
	}

	public function down()
	{
		$this->forge->dropColumn('package', 'details');
	}
}

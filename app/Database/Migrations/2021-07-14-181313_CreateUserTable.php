<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUserTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'first_name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'middle_name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'      => true,
            ],
            'last_name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'email'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'unique'    => true,
            ],
            'password'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'created_at datetime default current_timestamp',

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('user');
	}

	public function down()
	{
		$this->forge->dropDatabase('user');
	}
}

<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateOrderTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'transaction_id'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'amount'       => [
                'type'       => 'INT',
                'constraint' => 32,
            ],
            'package_id'       => [
                'type'       => 'INT',
                'constraint' => 4,
            ],
            'user_id'       => [
                'type'       => 'INT',
                'constraint' => 4,
            ],
            'usage_count'       => [
                'type'       => 'INT',
                'constraint' => 32,
            ],
            'extra'       => [
                'type'       => 'TEXT',
                'null'      => true
            ],
            'created_at datetime default current_timestamp',

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('order');
	}

	public function down()
	{
		$this->forge->dropTable('order');
	}
}

<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddMIMETypeUploadTable extends Migration
{
	public function up()
	{
        $fields = [
            'mime' => ['type' => 'VARCHAR', 'constraint' => '100', 'null' => false]
        ];
        $this->forge->addColumn('upload', $fields);
	}

	public function down()
	{
        $this->forge->dropColumn('upload', 'mime');
	}
}

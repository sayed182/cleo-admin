<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateAddressTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'user_id'    => [
                'type'           => 'INT',
                'constraint'     => 5,
            ],
            'city_name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'      => true
            ],
            'state_name' =>[
                "type"      => 'VARCHAR',
                'constraint'=> '100',
            ],
            'country_name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'      => true
            ],
            'address'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'zip_code'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100'
            ],
            'created_at datetime default current_timestamp',

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('address');
	}

	public function down()
	{
		$this->forge->dropTable('address');
	}
}

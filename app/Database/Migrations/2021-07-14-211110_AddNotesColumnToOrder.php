<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddNotesColumnToOrder extends Migration
{
	public function up()
	{
        $fields = [
            'notes' => ['type' => 'TEXT', 'null' => true]
        ];
        $this->forge->addColumn('order', $fields);
	}

	public function down()
	{
        $this->forge->dropColumn('order', 'notes');
	}
}

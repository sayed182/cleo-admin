<?= $this->section('title') ?>
- Admin | Upload review
<?php $this->endSection();?>

<?= $this->extend('components/template') ?>

<?= $this->section('content') ?>
    <main class="document-upload-form">
        <?= $this->include('components/alert') ?>
        <section class="document-upload-form__upload">
            <div class="container">
                <form action="<?= base_url('/admin/upload/'.$order_id)?>" id="uploader_form" class="scaffold-form" name="uploader_form" method="post"
                      enctype="multipart/form-data">
                    <div class="uploader_form">
                        <div class="fieldset">
                            <ul style="list-style: none;">
                                <li style="padding: 10px;">
                                    <div class="button-set">
                                        <h4 class="legend">
                                            Submit reviewed files
                                        </h4>
                                        <div class="ajax-upload-dragdrop upload-drag-and-drop-box">
                                            <h5 class="text-center image-name"></h5>
                                            <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438955603/file-upload-01.png"
                                                 alt="upload-file" />
                                            </br>
                                            <input type="file" name="document-upload" id="document-upload" style="display: none;"/>
                                            <p class="file-type-desc">( <font color="red">*</font>file extension
                                                allowed: <strong>.pdf, .docx, .png, .jpeg </strong>)</p>
                                            <div class="browse-button"
                                                 style="position: relative; overflow: hidden; cursor: default;">
                                                Browse
                                            </div>
                                        </div>


                                        <div class="" id="advice-required-entry-chooseFile" style="display: none;">You
                                            need to choose at least one file</div>

                                    </div>
                                    <div class="mx-auto d-block">
                                        <h5 class="text-center">Document Notes:</h5>
                                        <div class="col-md-6 offset-md-3">
                                            <textarea class="d-block w-100 mx-auto" name="document_notes" id="" rows="5"></textarea>
                                        </div>

                                    </div>

                        </div>
                        <div id="fileuploader" style="display: none;">
                            Choose file
                        </div>

                        <div class="" id="advice-required-entry-chooseFile" style="display: none;">
                            You need to choose at least one file
                        </div>



                    </div>
                    </form>
                    </ul>
                    <ul style="list-style: none;">
                        <li style="margin-left: 15px; margin-top: -25px;">
                            <div id="eventmessage" style="display: inline-block;">
                            </div>
                        </li>
                    </ul>
            </div>

            <div class="file-upload-buttons">
                <ul style="list-style: none;">
                    <li style="padding: 10px;">
                        <div class="add-to-cart">
                            <a href="<?= base_url('admin/services/'.$user_id);?>" class="form-back-button" type="button" id="goBackButton" name="btnGoBack" >
                                <span>
                                    <span>
                                        <p>
                                            <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438960670/back-button-icn.png"
                                                / class="animated rotateIn">
                                            Go back
                                        </p>
                                    </span>
                                </span>
                            </a>
                            &nbsp;&nbsp;
                            <button class="form-upload-button" type="submit" id="uploadFileButton" name="btnSubmit">
                                <span>
                                    <span id="uploadButtonName">
                                        <p>
                                            Upload
                                            <img src="https://res.cloudinary.com/dtutqsucw/image/upload/v1438960670/file-upload.png"
                                                / class="animated slideInUp">
                                        </p>
                                    </span>
                                </span>
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
            </div>

            </div>
        </section>

        <div class="container">
            <hr>
        </div>
    <div class="row">
        <div class="col-md-6 border-md-right">
            <section class="document-upload-form__download">
                <div class="container">
                    <h4 class="heading text-center">Document for Review</h4>
                    <small>Download documents uploaded by students.</small>
                    <div class="row">
                        <?php
                        if(isset($uploads) && count($uploads) > 0){
                            $i=0;
                            foreach ($uploads as $upload):
                                ?>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <?php if($i == 0){?>
                                            <span class="badge badge-pill badge-info">Latest</span>
                                        <?php } ?>
                                        <div class="card-body">
                                            <h4 class="catd-title text-center"><?= ++$i;?></h4>
                                            <p class="card-text">
                                                <span>Document Notes:</span><br>
                                                <?= $upload['upload_notes']?>
                                            </p>
                                            <hr>
                                            <a href="<?= base_url('/upload/get/'.$upload['path'])?>" target="_blank" class="download-icon">
                                                <i class="bi bi-arrow-down-circle"></i>
                                            </a>
                                            <h6 class="card-subtitle text-center "><?= $upload['file_name'] ?></h6>
                                            <small>Created At : <?= $upload['created_at']; ?></small>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; }else{?>
                            <h2 class="text-center text-info">No Documents Uploaded.</h2>
                        <?php }?>

                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-6">
            <section class="document-upload-form__download">
                <div class="container">
                    <h4 class="heading text-center">Reviewed Document</h4>
                    <small>Download documents already reviewed.</small>
                    <div class="row">
                        <?php
                        if(isset($reviewed)){
                            $i=0;
                            foreach ($reviewed as $upload):
                                ?>
                                <div class="col-md-6 mb-3">
                                    <div class="card">
                                        <?php if($i == 0){?>
                                        <span class="badge badge-pill badge-info">Latest</span>
                                        <?php } ?>
                                        <div class="card-body">
                                            <h4 class="catd-title text-center"><?= ++$i;?></h4>
                                            <p class="card-text">
                                                <span>Document Notes:</span><br>
                                                <?= $upload['upload_notes']?>
                                            </p>
                                            <hr>
                                            <a href="<?= base_url('/upload/get/'.$upload['path'])?>" target="_blank" class="download-icon">
                                                <i class="bi bi-arrow-down-circle"></i>
                                            </a>
                                            <h6 class="card-subtitle text-center "><?= $upload['file_name'] ?></h6>
                                            <small>Created At : <?= $upload['created_at']; ?></small>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; } else{?>
                            <h2 class="text-center text-info">No Documents Reviewed.</h2>
                        <?php }?>

                    </div>
                </div>
            </section>
        </div>
    </div>




    </main>

<?= $this->endSection() ?>

    <!--Load Extra Styles for this page-->
<?php $this->section('styles'); ?>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<?php $this->endSection(); ?>

    <!--Load Extra Scripts for this page-->
<?= $this->section('js_scripts') ?>
    <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(()=>{
            $('#document-upload').on('change', function(){
                var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
                $('.image-name').text(filename);
            })
            $("#uploadFileButton").on('click', ()=>{
                $("#uploader_form").submit();
            });
        });
    </script>
<?= $this->endSection() ?>
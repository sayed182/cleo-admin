<?= $this->section('title') ?>
    - Admin | Add package
<?php $this->endSection();?>

<?= $this->extend('components/template') ?>

<?= $this->section('content') ?>
<?= $this->include('components/alert') ?>
<?php $id = isset($_GET['pid'])?$_GET['pid']:''; ?>
<div class="container py-5">
    <form action="<?= base_url('/admin/package')?>" method="post">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Package Name</label>
                    <input type="text" name="package-name" class="form-control" value="<?php echo isset($package['name'])?$package['name'] :''?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Package Price</label>
                    <input type="text" name="package-price" class="form-control" value="<?php echo isset($package['price'])?$package['price'] :''?>">
                </div>
            </div><div class="col-md-2">
                <div class="form-group">
                    <label for="">Package Usage Count</label>
                    <input type="number" name="package-usage" class="form-control" value="<?php echo isset($package['usage_count'])?$package['usage_count'] :''?>">
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <label for="">Package Highlight Color</label>
                    <input type="color" name="package-color" class="form-control" value="<?php echo isset($package['color'])?$package['color'] :''?>">
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Package Content</label>
                    <textarea name="package-atts" id="div_editor1" cols="30" rows="10">
                        <?php echo isset($package['details'])?$package['details'] :''?>
                    </textarea>
                </div>
            </div>
        </div>
        <input type="hidden" name="package-id" value="<?= $id; ?>">
        <button type="submit" class="btn btn-submit">Submit</button>




    </form>
</div>



<?= $this->endSection() ?>

<!--Load Extra Styles for this page-->
<?php $this->section('styles'); ?>
<link rel="stylesheet" href="<?= base_url('/css/rte_theme_default.css')?>">
<?php $this->endSection(); ?>

<!--Load Extra Scripts for this page-->
<?= $this->section('js_scripts') ?>
<script src="<?= base_url('/js/rte.js')?>"></script>
<script src="<?= base_url('/js/plugins/all_plugins.js')?>"></script>
<script>
    var editor1 = new RichTextEditor("#div_editor1");

</script>
<?= $this->endSection() ?>

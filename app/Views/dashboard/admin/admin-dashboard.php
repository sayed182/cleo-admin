<?= $this->section('title') ?>
 - Admin | Dashboard
<?php $this->endSection();?>

<?= $this->extend('components/template') ?>

<?= $this->section('content') ?>

<main class="students-list">


    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <input type="text" class="form-control" id="searchByName" placeholder="Type to search">
            </div>
            <div class="col-md-3">
                <form action="<?= base_url('/admin/')?>" method="get" id="service-count-filter-form">
                    <select name="service-count" id="service-count-filter" class="form-control">
                        <option value="all" <?= (isset($_GET['service-count'])&&$_GET['service-count']=="all")? 'selected':''; ?> >All</option>
                        <option value="yes" <?= (isset($_GET['service-count'])&&$_GET['service-count']=="yes")? 'selected':''; ?>>Have One or More Services Bought</option>
                        <option value="no" <?= (isset($_GET['service-count'])&&$_GET['service-count']=="no")? 'selected':''; ?>>Have Zero Services Bought</option>
                    </select>
                </form>

            </div>
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <a href="<?= base_url('/admin/export_xls')?>" target="_blank">Export as Excel</a>
            </div>

        </div>
        <div class="row">
            <?php foreach ($users as $user): ?>

            <div class="col-md-3 card-container">
                <div class="card">
                    <div class="card-body">

                        <a href="<?= base_url().'/admin/services/'.$user['id'] ?>" class="students-list--icon">
                            <i class="bi bi-person"></i>
                        </a>
                        <h5 class="card-title name"><?= $user['last_name']?>, <?= $user['first_name']?></h5>
                        <h6 class="pt-1 d-block email"><?= $user['email']?></h6>
                        <small class="pt-1 d-block">Services Bought :<span class="service-bought-count"><?= $user['service_count']?></span></small>
                        <small class="pt-1 d-block"><span style="color: #a09f9f;">Last bought on :</span>
                            <?= isset($user['last_ordered_on'])? explode(' ',$user['last_ordered_on'])[0] : "" ?>
                        </small>
                        <small class="pt-1 d-block"><span style="color: #a09f9f;">Last uploaded on: </span>
                            <?= isset($user['last_uploaded_on'])? explode(' ',$user['last_uploaded_on'])[0] : "" ?>
                        </small>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>


        </div>
</main>


<?= $this->endSection() ?>

    <!--Load Extra Styles for this page-->
<?php $this->section('styles'); ?>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<?php $this->endSection(); ?>

    <!--Load Extra Scripts for this page-->
<?= $this->section('js_scripts') ?>
    <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script>
        $("#searchByName").on('keyup', function (e){
            var searchText = new RegExp(e.target.value, 'i');
            console.log(searchText);
            $('.card-container').hide();
            $('.card-body').each(function(index,ele){
                if($(this).text().search(searchText) > -1){
                    var card = $(ele).parent().parent();
                    card.show();
                }
            })
        });
        $("#service-count-filter").on('change', function (){
            $("#service-count-filter-form").submit();
        })



    </script>
<?= $this->endSection() ?>
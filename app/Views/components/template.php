<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('/favicon.png') ?>"/>
    <link rel="shortcut icon" type="image/x-icon" href="https://cleoinc.org/wp-content/uploads/2015/12/CLEO-INC-fav-16x16.png">
    <title>Cleo <?= $this->renderSection('title') ?></title>
    <?= $this->renderSection('styles') ?>
    <link rel="stylesheet" href="<?php echo base_url("/css/alert.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("/css/main.css")?>">

</head>
<?php
?>
<body>
<header>
    <div class="sub-nav">
        <div class="container">
            <ul class="sub-nav_list">
                <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-telephone-fill"></i>
                        </span>
                    <span class="nav-text">
240-582-8600
                        </span>
                </li>
                <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-geo-alt-fill"></i>
                        </span>
                    <span class="nav-text">1101 Mercantile Ln., Suite 294, Largo, MD 20774
</span>
                </li>
                <li class="sub-nav_list--item">

                    <a class="nav-icon" href="http://www.facebook.com/cleo.scholars" target="_blank">
                        <i class="bi bi-facebook"></i>
                    </a>
                    <a class="nav-icon" href="http://twitter.com/cleoscholars" target="_blank">
                        <i class="bi bi-twitter"></i>
                    </a>
                    <a class="nav-icon" href="https://www.instagram.com/_cleoscholars_" target="_blank">
                        <i class="bi bi-instagram"></i>
                    </a>
                    <a class="nav-icon" href="https://www.linkedin.com/in/cleo1968" target="_blank">
                        <i class="bi bi-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg ">
        <div class="container">

            <a class="navbar-brand" href="https://cleoinc.org"><img src="<?php echo base_url("/images/logo-expanded.png");?>" class="img-responsive"
                                                  alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler"
                    aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse right-navbar" id="navbarToggler">
                <?php
                    $session = session();
                    if(isset($session->get('isLoggedIn')['user'])){
                    $data =$session->get('isLoggedIn');
                    $user = $data['user'];
                ?>
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="https://cleoinc.org">
                            Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="https://cleoinc.org/programs/clas/" >

                            CLAS
                        </a>
                    </li>
                    <li class="nav-item">
                        <?php if(isset($session->get('isLoggedIn')['is_admin']) && $session->get('isLoggedIn')['is_admin']) {?>
                        <a class="nav-link" href="<?= base_url().'/admin/'?>" >
                            <?php }else{?>
                            <a class="nav-link" href="<?= base_url().'/dashboard/'?>" >
                                <?php }?>
                                <span class="nav-link_icon">
                                <i class="bi bi-house"></i>
                            </span>
                                Dashboard
                            </a>
                    </li>
                    <li class="nav-item">
                        <?php if(isset($session->get('isLoggedIn')['is_admin']) && $session->get('isLoggedIn')['is_admin']) {?>
                        <a class="nav-link" href="<?= base_url().'/admin/'?>" >
                        <?php }else{?>
                        <a class="nav-link" href="<?= base_url().'/dashboard/'?>" >
                        <?php }?>
                            <span class="nav-link_icon">
                                <i class="bi bi-person-fill"></i>
                            </span>
                            <?php echo $user['first_name'].' '.$user['last_name']; ?>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url().'/home/logout'?>" >
                <span class="nav-link_icon">
                    <i class="bi bi-box-arrow-right"></i>
                </span>
                            Logout
                        </a>
                    </li>
                </ul>
                <?php
                    }else{
                ?>
                <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="https://cleoinc.org">
                            Home
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="https://cleoinc.org/pre-law-programs/" >

                            PRE-LAW PROGRAMS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#" data-toggle="modal"
                           data-target-modal="admin-login-modal">
                        <span class="nav-link_icon">
                            <i class="bi bi-house-fill"></i>
                        </span>
                            Admin Login
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="modal" data-target-modal="student-login-modal">
                        <span class="nav-link_icon">
                            <i class="bi bi-person-fill"></i>
                        </span>
                            Student Login</a>
                    </li>
                </ul>

                <?php
                    }
                ?>

            </div>
        </div>
    </nav>
</header>


<?= $this->renderSection('content') ?>


<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="copyright">
                    <p class="copyright-text">Copyright © 2021 Council on Legal Education Opportunity (CLEO) Inc</p>
                </div>

            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-12">
                        <ul class="footer-social-list">
                            <li>
                                <a class=""  href="http://www.facebook.com/cleo.scholars" target="_blank">
                                    <i class="bi bi-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="" href="http://twitter.com/cleoscholars" target="_blank">
                                    <i class="bi bi-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="" href="https://www.instagram.com/_cleoscholars_" target="_blank">
                                    <i class="bi bi-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a class="" href="https://www.linkedin.com/in/cleo1968" target="_blank">
                                    <i class="bi bi-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <ul class="footer-nav">
                            <li class="footer-nav_list"><a href="https://cleoinc.org/pre-law-programs/" target="_blank" class="footer-nav_list--item">PRE-LAW PROGRAMS</a></li>
                            <li class="footer-nav_list"><a href="https://cleoinc.org/contact/" target="_blank" class="footer-nav_list--item">CONTACT</a></li>
                            <li class="footer-nav_list"><a href="https://cleoinc.org/donate/" target="_blank" class="footer-nav_list--item">DONATE</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<?= $this->include('components/student-login-modal'); ?>
<?= $this->include('components/admin-login-modal'); ?>

<script src="<?php echo base_url("/js/jquery.min.js")?>"></script>

<?= $this->renderSection('js_scripts') ?>

<script src="<?php echo base_url("/js/main.js")?>"></script>
<script>
    if($('.alert').length > 0){
        setTimeout(function(){
            $('.alert').fadeOut();
        },1000 * 60 );
    }
    $('.alert .bi-x-lg').on('click', function(){
        $('.alert').fadeOut();
        $.ajax({
            url:"<?= base_url('/home/clearSession')?>",
            method:'get',
            success: function(){
                console.log("Session Cleared");
            }
        })
    })


</script>

</body>

</html>

<?php


namespace App\Helpers;


class EmailHelper
{
    public function send_mail($to, $name, $file)
    {
        $email = \Config\Services::email();
        $email->setNewline("\r\n");
        $email->setFrom('cleo@cleoinc.org');
        $email->setTo($to);
        $email->setSubject("Download File");
        $email->setPriority(1);
        $email->setReplyTo('cleo@cleoinc.org');
        $msg = view('email/new-upload',['user_name'=>$name,]);
        $email->setMessage($msg);
        $mail = $email->send();
        if($mail){
//            print_r($to);
            return $msg;
        }else{
//            print_r($email->printDebugger(['headers']));
//            die();
            return "Not sent";
        }
    }
    public function send_mail_to_admin($name, $file)
    {
        $email = \Config\Services::email();
        $email->setNewline("\r\n");
        $email->setFrom('clas@cleoinc.org');
        $email->setTo('cleo@cleoinc.org');
        // $email->setTo('sayed182@gmail.com');
        $email->setSubject("New File to Clas.Cleoinc.org Uploaded by ".$name);
        $email->setPriority(1);
        $msg = view('email/new-upload-by-user',['user_name'=>$name,]);
        $email->setMessage($msg);
        $mail = $email->send();
        if($mail){
//            print_r($to);
            return $msg;
        }else{
//            print_r($email->printDebugger(['headers']));
//            die();
            return "Not sent";
        }
    }
    public function send_reset_mail($to, $token)
    {
        $link = base_url('/home/setpassword?token='.$token);
        $email = \Config\Services::email();
        $email->setNewline("\r\n");
        $email->setFrom('cleo@cleoinc.org');
        $email->setTo($to);
        $email->setSubject("Password Reset from clas.cleonic.org");
        $email->setPriority(1);
        $msg = view('email/password-reset',['link'=>$link]);
        $email->setMessage($msg);
        $mail = $email->send();
        if($mail){
            return $msg;
        }else{
//            print_r($email->printDebugger(['headers']));
//            die();
            return "Not sent";
        }
    }

    public function send_successfull_registered($user_name, $user_email, $mobile)
    {
        $email = \Config\Services::email();
        $email->setNewline("\r\n");
        $email->setFrom('clas@cleoinc.org');
        $email->setTo("cleo@cleoinc.org");
        // $email->setTo('sayed182@gmail.com');
        $email->setSubject("A new user has Successfully Registered.");
        $email->setPriority(1);
        $msg = view('email/new-registration', ['user_name'=> $user_name,'email'=> $user_email,'phone_number'=>$mobile]);
        $email->setMessage($msg);
        $mail = $email->send();
        if($mail){
            return $msg;
        }else{
//            print_r($email->printDebugger(['headers']));
//            die();
            return "Not sent";
        }
    }
    public function send_payment_successful( $firstname, $lastname, $user_email, $price, $package_name)
    {
        $email = \Config\Services::email();
        $email->setNewline("\r\n");
        $email->setFrom('clas@cleoinc.org');
        $email->setTo("cleo@cleoinc.org");
        // $email->setTo('sayed182@gmail.com');
        $email->setSubject("Payment Successful.");
        $email->setPriority(1);
        $msg = view('email/payment-success', ['first_name'=> $firstname, 'last_name'=> $lastname, 'user_email'=> $user_email,'price'=>$price, 'package_name' =>$package_name]); 
        $email->setMessage($msg);
        $mail = $email->send();
        if($mail){
            return $msg;
        }else{
//            print_r($email->printDebugger(['headers']));
//            die();
            return "Not sent";
        }
    }

}
<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UploadModel;

class Upload extends BaseController
{
	public function get($folder_name,$file_name)
	{
        if(($file = file_get_contents(WRITEPATH.'uploads/'.$folder_name.'/'.$file_name)) === FALSE){
            return view('errors/404', ['message' => "File not found", 'code' => '500']);
        }

        $upload = new UploadModel();
        $upload = $upload->where('path', $folder_name.'/'.$file_name)->first();
//        dd($folder_name, $file_name);
        $mimeType = $upload['mime'];
        $this->response
            ->setStatusCode(200)
            ->setContentType($mimeType)
            ->setBody($file)
            ->send();
	}
}

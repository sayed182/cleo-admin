<?= $this->section('title') ?>
 - Admin | Services Bought List
<?php $this->endSection();?>

<?= $this->extend('components/template') ?>

<?= $this->section('content') ?>
    <main class="my-transactions">
        <div class="container">
            <h4 class="heading">Scholar Programs</h4>

            <table id="admin-overview-table" class="display" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Transaction ID</th>
                    <th>Document Type</th>
                    <th>Date</th>
                    <th></th>

                </tr>
                </thead>
                <tbody>
                <?php
                if(count($orders)>0):
                    $i=0;
                    foreach ($orders as $order):
                    ?>
                <tr>
                    <td><?= ++$i; ?></td>
                    <td><?= $order['transaction_id'] ?></td>
                    <td><?= $order['package_name'] ?></td>
                    <td><?= $order['created_at'] ?></td>
                    <td>
                        <a class="btn btn-block" href="<?= base_url().'/admin/upload/'.$order['id'] ?>">Upload / Download</a>
                    </td>


                </tr>
                <?php    endforeach;
                endif;
                ?>


                </tbody>
            </table>
        </div>
    </main>


<?= $this->endSection() ?>

    <!--Load Extra Styles for this page-->
<?php $this->section('styles'); ?>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<?php $this->endSection(); ?>

    <!--Load Extra Scripts for this page-->
<?= $this->section('js_scripts') ?>
    <script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<?= $this->endSection() ?>
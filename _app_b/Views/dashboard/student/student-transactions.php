<?= $this->section('title') ?>
 - Student Transactions
<?php $this->endSection();?>

<?= $this->extend('components/template') ?>

<?= $this->section('content') ?>

<main class="my-transactions">
    <div class="container">
        <?php
//            print_r($transactions)
        ?>
        <h4 class="heading">List of Transactions</h4>
        <table id="student-transaction-table" class="display" style="width:100%">
            <thead>
            <tr>
                <th>Number</th>
                <th>Transaction ID</th>
                <th>Pacakage Selected</th>
                <th>Package Upload Limits</th>
                <th>Transaction date</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
                $i=0;
                foreach ($transactions as $transaction):
                    $date = new DateTime($transaction['created_at']);
            ?>
            <tr>
                <td><?= ++$i; ?></td>
                <td><?= $transaction['transaction_id'];?></td>
                <td><?= $transaction['package_name'];?></td>
                <td><?= $transaction['usage_count'];?>/ <?= $transaction['p_usage_count'];?> </td>
                <td><?= $date->format('Y/m/d')?></td>
                <td>
                    <a class="btn btn-block" href="<?= base_url().'/dashboard/student-upload/'.$transaction['id']?>">Upload / Download</a>
                </td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</main>


<?= $this->endSection() ?>

<!--Load Extra Styles for this page-->
<?php $this->section('styles'); ?>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<?php $this->endSection(); ?>

<!--Load Extra Scripts for this page-->
<?= $this->section('js_scripts') ?>
<script src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<?= $this->endSection() ?>

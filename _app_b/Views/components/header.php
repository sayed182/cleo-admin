<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cleo</title>
    <link rel="stylesheet" href="./css/main.css">
</head>

<body>
    <header>
        <div class="sub-nav">
            <div class="container">
                <ul class="sub-nav_list">
                    <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-telephone-fill"></i>
                        </span>
                        <span class="nav-text">
240-582-8600
                        </span>
                    </li>
                    <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-geo-alt-fill"></i>
                        </span>
                        <span class="nav-text">1101 Mercantile Ln., Suite 294, Largo, MD 20774
</span>
                    </li>
                    <li class="sub-nav_list--item">

                        <a class="nav-icon">
                            <i class="bi bi-facebook"></i>
                        </a>
                        <a class="nav-icon">
                            <i class="bi bi-twitter"></i>
                        </a>
                        <a class="nav-icon">
                            <i class="bi bi-instagram"></i>
                        </a>
                        <a class="nav-icon">
                            <i class="bi bi-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg ">
            <div class="container">

                <a class="navbar-brand" href="#"><img src="./images/logo-expanded.png" class="img-responsive"
                        alt=""></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler"
                    aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse right-navbar" id="navbarToggler">

                    <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#" data-toggle="modal"
                                data-target-modal="admin-login-modal">
                                <span class="nav-link_icon">
                                    <i class="bi bi-house-fill"></i>
                                </span>
Admin
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target-modal="student-login-modal">
                                <span class="nav-link_icon">
                                    <i class="bi bi-person-fill"></i>
                                </span>
Student</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </header>
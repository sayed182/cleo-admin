<div class="modal fade" id="student-login-modal">
    <div class="modal-stage"></div>
    <div class="modal-body">
            <span id="close-button">
                <i class="bi bi-x-lg"></i>
            </span>
        <form action="<?= base_url().'/home/login'?>" method="post" class="login-modal" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="login-email" class="form-label">Email address</label>
                <input type="email" class="form-control" id="loginemail" name="login-email" placeholder="name@example.com"
                       autocomplete="username" required>
            </div>
            <div class="mb-3">
                <label for="login-password" class="form-label">Password</label>
                <input type="password" class="form-control" id="login-password" name="login-password" placeholder=""
                       autocomplete="current-password" required>
            </div>
            <div class="mb-3">
                <input type="submit" class="btn btn-submit" id="login-submit" placeholder="Login" value="Login"
                       readonly>
            </div>
            <a class="text-primary text-small" href="<?= base_url('/home/forgetpassword')?>"><small>Forget Password ? Click here to reset.</small></a>
        </form>
    </div>
</div>
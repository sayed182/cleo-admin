<?php $session = session();?>
<?php if($session->getFlashdata('type') == "success") {?>
    <div class="alert alert-success">
        <div class="icon__wrapper">
            <span class="bi bi-exclamation-triangle"></span>
        </div>
        <p><?= $session->getFlashdata('message')?></p>
        <span class="mdi mdi-open-in-new open"></span>
        <span class="bi bi-x-lg"></span>
    </div>

<?php } ?>

<?php if($session->getFlashdata('type') == "warning") {?>
    <div class="alert alert-warning">
        <div class="icon__wrapper">
            <span class="bi bi-exclamation-triangle"></span>
        </div>
        <p><?= $session->getFlashdata('message')?></p>
        <span class="mdi mdi-open-in-new open"></span>
        <span class="bi bi-x-lg"></span>
    </div>

<?php } ?>

<?php if($session->getFlashdata('type') == "error") {?>
    <div class="alert alert-error">
        <div class="icon__wrapper">
            <span class="bi bi-exclamation-triangle"></span>
        </div>
        <p><?= $session->getFlashdata('message')?></p>
        <span class="mdi mdi-open-in-new open"></span>
        <span class="bi bi-x-lg"></span>
    </div>
<?php } ?>



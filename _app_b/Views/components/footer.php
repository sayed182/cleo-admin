

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="copyright">
                    <p class="copyright-text">Copyright © 2021 Council on Legal Education Opportunity (CLEO) Inc</p>
                </div>

            </div>
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-12">
                        <ul class="footer-social-list">
                            <li>
                                <a class="">
                                    <i class="bi bi-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a class="">
                                    <i class="bi bi-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a class="">
                                    <i class="bi bi-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a class="">
                                    <i class="bi bi-linkedin"></i>
                                </a>
                            </li>
                            </ul>
                    </div>
                    <div class="col-12">
                        <ul class="footer-nav">
                            <li class="footer-nav_list"><a href="#" class="footer-nav_list--item">PRE-LAW PROGRAMS</a></li>
                            <li class="footer-nav_list"><a href="#" class="footer-nav_list--item">CONTACT</a></li>
                            <li class="footer-nav_list"><a href="#" class="footer-nav_list--item">DONATE</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<script src="/js/jquery.min.js"></script>
<script src="/js/main.js"></script>
</body>

</html>


<?= $this->extend('components/template') ?>
<?= $this->section('content') ?>
<?php
$session = session();
?>
<main class="subsciption-form">
    <?php $this->include('components/alert'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 subsciption-form_container">
                <?php
                if(isset($session->get('isLoggedIn')['user'])){?>
                    <?= $this->section('title') ?>
                    - Complete you payment
                    <?php $this->endSection();?>
                    <h4 class="text-center">You are purchasing:</h4>
                    <h3 class="text-center"><?= $package["name"]?></h3>
                    <h3 class="text-center">Price: $<?= $package["price"]?></h3>


                    <form method="post" action="<?= base_url().'/home/register/'.$id ?>" enctype="multipart/form-data">


                        <input type="hidden" name="package_price" value="<?= $package["price"]?>" />
                        <input type="hidden" name="package_id" value="<?= $package["id"]?>" />
                        <input type="hidden" name="package_name" value="<?= $package["name"]?>" />

                        <button type="submit" class="btn btn-submit">Proceed to payment</button>
                    </form>
                <?php }else{ ?>
                    <?= $this->section('title') ?>
                    - Register for a service
                    <?php $this->endSection();?>
                <h4 class="text-center">Please provide the complete the following information</h4>
                <form method="post" action="<?= base_url().'/home/register/'.$id ?>" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="firstname" class="form-label">First Name</label>
                                <input type="text" class="form-control" id="firstname" name="first_name"
                                       aria-describedby="emailHelp" required>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="middlename" class="form-label">Middle Name</label>
                                <input type="text" class="form-control" id="middlename" name="middle_name"
                                       aria-describedby="emailHelp">

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="lastname" class="form-label">Last Name</label>
                                <input type="text" class="form-control" id="lastname" name="last_name"
                                       aria-describedby="emailHelp" required>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="mb-3">
                                <label for="mobile" class="form-label">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile"
                                       aria-describedby="mobileHelp" name="mobile" required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="city" class="form-label">City</label>
                                <input type="text" class="form-control" id="city" name="city"
                                       aria-describedby="emailHelp" required>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="firstname" class="form-label">State</label>
                                <select class="form-control" id="state" name="state" required>
                                    <!--<option value=''>Select</option>-->
                                    <option value="UNK">Outside
                                        US / Canada </option>
                                    <option value="AL">Alabama
                                    </option><option value="AK">Alaska
                                    </option>
                                    <option value="AB">Alberta
                                    </option>
                                    <option value="AS">American
                                        Samoa </option>
                                    <option value="AZ">Arizona
                                    </option>
                                    <option value="AR">Arkansas
                                    </option>
                                    <option value="AA">Armed
                                        Forces Americas </option>
                                    <option value="AE">Armed
                                        Forces Europe </option>
                                    <option value="AP">Armed
                                        Forces Pacific </option>
                                    <option value="BC">British
                                        Columbia </option>
                                    <option value="CA">California
                                    </option>
                                    <option value="CO">Colorado
                                    </option>
                                    <option value="CT">Connecticut
                                    </option>
                                    <option value="DE">Delaware
                                    </option>
                                    <option value="DC">District
                                        Of Columbia </option>
                                    <option value="FL">Florida
                                    </option>
                                    <option value="GA">Georgia
                                    </option>
                                    <option value="GU">Guam
                                    </option>
                                    <option value="HI">Hawaii
                                    </option>
                                    <option value="ID">Idaho
                                    </option>
                                    <option value="IL">Illinois
                                    </option>
                                    <option value="IN">Indiana
                                    </option>
                                    <option value="IA">Iowa
                                    </option>
                                    <option value="KS">Kansas
                                    </option>
                                    <option value="KY">Kentucky
                                    </option>
                                    <option value="LA">Louisiana
                                    </option>
                                    <option value="ME">Maine
                                    </option>
                                    <option value="MB">Manitoba
                                    </option>
                                    <option value="MD">Maryland
                                    </option>
                                    <option value="MA">Massachusetts
                                    </option>
                                    <option value="MI">Michigan
                                    </option>
                                    <option value="MN">Minnesota
                                    </option>
                                    <option value="MS">Mississippi
                                    </option>
                                    <option value="MO">Missouri
                                    </option>
                                    <option value="MT">Montana
                                    </option>
                                    <option value="NE">Nebraska
                                    </option>
                                    <option value="NV">Nevada
                                    </option>
                                    <option value="NB">New Brunswick
                                    </option>
                                    <option value="NH">New Hampshire
                                    </option>
                                    <option value="NJ">New Jersey
                                    </option>
                                    <option value="NM">New Mexico
                                    </option>
                                    <option value="NY">New York
                                    </option>
                                    <option value="NF">Newfoundland
                                    </option>
                                    <option value="NC">North
                                        Carolina </option>
                                    <option value="ND">North
                                        Dakota </option>
                                    <option value="MP">Northern
                                        Mariana Is </option>
                                    <option value="NT">Northwest
                                        Territories </option>
                                    <option value="NS">Nova
                                        Scotia </option>
                                    <option value="OH">Ohio
                                    </option>
                                    <option value="OK">Oklahoma
                                    </option>
                                    <option value="ON">Ontario
                                    </option>
                                    <option value="OR">Oregon
                                    </option>
                                    <option value="PW">Palau
                                    </option>
                                    <option value="PA">Pennsylvania
                                    </option>
                                    <option value="PE">Prince
                                        Edward Island </option>
                                    <option value="PQ">Province
                                        du Quebec </option>
                                    <option value="PR">Puerto
                                        Rico </option>
                                    <option value="RI">Rhode
                                        Island </option>
                                    <option value="SK">Saskatchewan
                                    </option>
                                    <option value="SC">South
                                        Carolina </option>
                                    <option value="SD">South
                                        Dakota </option>
                                    <option value="TN">Tennessee
                                    </option>
                                    <option value="TX">Texas
                                    </option>
                                    <option value="UT">Utah
                                    </option>
                                    <option value="VT">Vermont
                                    </option>
                                    <option value="VI">Virgin
                                        Islands </option>
                                    <option value="VA">Virginia
                                    </option>
                                    <option value="WA">Washington
                                    </option>
                                    <option value="WV">West
                                        Virginia </option>
                                    <option value="WI">Wisconsin
                                    </option>
                                    <option value="WY">Wyoming
                                    </option>
                                    <option value="YT">Yukon
                                        Territory </option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="mb-3">
                                <label for="zip" class="form-label">Zip</label>
                                <input type="text" class="form-control" id="zip" name="zip"
                                       aria-describedby="emailHelp" required>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email"
                                       aria-describedby="emailHelp" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="mb-3">
                                <label for="password" class="form-label">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       aria-describedby="emailHelp" required>

                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="package_price" value="<?= $package["price"]?>" />
                    <input type="hidden" name="package_id" value="<?= $package["id"]?>" />
                    <input type="hidden" name="package_name" value="<?= $package["name"]?>" />

                    <button type="submit" class="btn btn-submit">Submit</button>
                </form>
                <?php } ?>
            </div>
        </div>

    </div>

</main>

<?= $this->endSection() ?>

<?= $this->section('js_scripts') ?>
<script>
    $("#confirmpassword").on('keyup', function (e){
        var cf_password = e.target.value;
        var pasword = $("#password").val()
        if (pasword != cf_password){
            $(this).addClass('border border-danger');
        }else{
            $(this).removeClass('border border-danger');
        }
    });




</script>
<?= $this->endSection() ?>

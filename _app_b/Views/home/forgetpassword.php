
<?= $this->extend('components/template') ?>
<?= $this->section('content') ?>

<?= $this->section('title') ?>
- Password Reset Form
<?php $this->endSection();?>
<main class="subsciption-form">
    <?= $this->include('components/alert') ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 subsciption-form_container">



                <form method="post" action="<?= base_url().'/home/forgetpassword' ?>" enctype="multipart/form-data">

                    <div class="col-md-12 col-sm-12">
                        <div class="mb-3">
                            <label for="email" class="form-label">Registered Email ID:</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   aria-describedby="emailHelp" required>

                        </div>
                    </div>

                    <button type="submit" class="btn btn-submit">Submit reset request</button>
                </form>

            </div>
        </div>

    </div>

</main>

<?= $this->endSection() ?>
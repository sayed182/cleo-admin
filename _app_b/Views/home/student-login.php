<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('/favicon.png') ?>"/>
    <link rel="shortcut icon" type="image/x-icon" href="https://cleoinc.org/wp-content/uploads/2015/12/CLEO-INC-fav-16x16.png">
    <title>Cleo <?= $this->renderSection('title') ?></title>
    <?= $this->renderSection('styles') ?>
    <link rel="stylesheet" href="<?php echo base_url("/css/alert.css")?>">
    <link rel="stylesheet" href="<?php echo base_url("/css/main.css")?>">

</head>
<?php
?>
<body>
<header>
    <div class="sub-nav">
        <div class="container">
            <ul class="sub-nav_list">
                <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-telephone-fill"></i>
                        </span>
                    <span class="nav-text">
240-582-8600
                        </span>
                </li>
                <li class="sub-nav_list--item">
                        <span class="nav-icon">
                            <i class="bi bi-geo-alt-fill"></i>
                        </span>
                    <span class="nav-text">1101 Mercantile Ln., Suite 294, Largo, MD 20774
</span>
                </li>
                <li class="sub-nav_list--item">

                    <a class="nav-icon" href="http://www.facebook.com/cleo.scholars" target="_blank">
                        <i class="bi bi-facebook"></i>
                    </a>
                    <a class="nav-icon" href="http://twitter.com/cleoscholars" target="_blank">
                        <i class="bi bi-twitter"></i>
                    </a>
                    <a class="nav-icon" href="https://www.instagram.com/_cleoscholars_" target="_blank">
                        <i class="bi bi-instagram"></i>
                    </a>
                    <a class="nav-icon" href="https://www.linkedin.com/in/cleo1968" target="_blank">
                        <i class="bi bi-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg ">
        <div class="container">

            <a class="navbar-brand" href="https://cleoinc.org"><img src="<?php echo base_url("/images/logo-expanded.png");?>" class="img-responsive"
                                                  alt=""></a>
            
                                                  <h1>Student Login</h1>

        </div>
    </nav>
</header>  
    <?= $this->include('components/alert') ?>
    <main class="">
        
        <section class="program-showcase pt-5">
            <div class="container">
                <div class="card" style="max-width:45rem; margin:0 auto;"> 
                    <div class="card-body">
                    <form action="<?= base_url().'/home/login'?>" method="post" class="login-modal" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="login-email" class="form-label">Email address</label>
                <input type="email" class="form-control" id="loginemail" name="login-email" placeholder="name@example.com"
                       autocomplete="username" required>
            </div>
            <div class="mb-3">
                <label for="login-password" class="form-label">Password</label>
                <input type="password" class="form-control" id="login-password" name="login-password" placeholder=""
                       autocomplete="current-password" required>
            </div>
            <div class="mb-3">
                <input type="submit" class="btn btn-submit" id="login-submit" placeholder="Login" value="Login"
                       readonly>
            </div>
            <a class="text-primary text-small" href="<?= base_url('/home/forgetpassword')?>"><small>Forget Password ? Click here to reset.</small></a>
        </form>
                    </div>

                </div>
            
            </div>

        </section>
    </main>

    


<script src="<?php echo base_url("/js/main.js")?>"></script>
<script>
    if($('.alert').length > 0){
        setTimeout(function(){
            $('.alert').fadeOut();
        },1000 * 60 );
    }
    $('.alert .bi-x-lg').on('click', function(){
        $('.alert').fadeOut();
        $.ajax({
            url:"<?= base_url('/home/clearSession')?>",
            method:'get',
            success: function(){
                console.log("Session Cleared");
            }
        })
    })


</script>

</body>

</html>
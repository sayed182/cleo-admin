
<?= $this->extend('components/template') ?>
<?= $this->section('content') ?>

<?= $this->section('title') ?>
- Password Reset Form
<?php $this->endSection();?>
<main class="subsciption-form">
    <?= $this->include('components/alert') ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 subsciption-form_container">



                <form method="post" action="<?= base_url().'/home/setpassword' ?>" enctype="multipart/form-data">
                    <input type="hidden" name="token" value="<?= isset($token)?$token:""; ?>">
                    <div class="col-md-12 col-sm-12">
                        <div class="mb-3">
                            <label for="password" class="form-label">Enter your new Password:</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   aria-describedby="emailHelp" required>

                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="mb-3">
                            <label for="cpassword" class="form-label">Confirm Password:</label>
                            <input type="password" class="form-control" id="cpassword" name="cpassword"
                                   aria-describedby="emailHelp" required>

                        </div>
                    </div>

                    <button type="submit" class="btn btn-submit">Submit reset request</button>
                </form>

            </div>
        </div>

    </div>

</main>

<?= $this->endSection() ?>
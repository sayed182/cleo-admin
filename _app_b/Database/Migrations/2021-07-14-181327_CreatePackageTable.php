<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreatePackageTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'price'       => [
                'type'       => 'INT',
                'constraint' => 32,
            ],
            'usage_count'       => [
                'type'       => 'INT',
                'constraint' => 4,

            ],
            'is_active'       => [
                'type'       => 'BOOLEAN',
                'default'   => true
            ],
            'created_at datetime default current_timestamp',

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('package');
	}

	public function down()
	{
		$this->forge->dropTable('package');
	}
}

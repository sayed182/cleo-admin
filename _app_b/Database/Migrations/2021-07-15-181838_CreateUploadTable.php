<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUploadTable extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id'          => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'path'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'name'       => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => true
            ],
            'uploaded_by'       => [
                'type'       => 'enum',
                'constraint'     => ['user', 'admin'],
                'default'        => 'user',
            ],

            'created_at datetime default current_timestamp',

        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('upload');
	}

	public function down()
	{
        $this->forge->dropTable('upload');
	}
}

<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Helpers\EmailHelper;
use App\Helpers\PaymentHelper;
use App\Models\OrderModel;
use App\Models\PacakageModel;
use App\Models\UserModel;
use CodeIgniter\Database\Exceptions\DatabaseException;

class Payment extends BaseController
{

    public function success(){
        $session = session();
        $orderModel = new OrderModel();
        $packageModel = new PacakageModel();
        $userModel = new UserModel();
        $payment_id = $this->request->getGet('paymentId');
        $token = $this->request->getGet('token');
        $payer_id = $this->request->getGet('PayerID');
        $gateway = new PaymentHelper();
        $response = $gateway->complete(array(
            'payer_id'             => $payer_id,
            'transactionReference' => $payment_id,
        ));

        if($response->isSuccessful()){
            $data = $response->getData();
            $transactions = $data['transactions'];
            $t_data =[];
            $i=0;
            foreach ($transactions as $transaction){
                unset($transaction['related_resources']);
                $t_data[$i++] = $transaction;
            }
            $details = $session->get('transaction_detail');

            $orderData = [
                'transaction_id' => $data['id'],
                'package_id'     => $details['package_id'],
                'user_id'        => $details['user_id'],
                'usage_count'    => 0,
                'extra'          => serialize($t_data)

            ];
            try{
                $order_id = $orderModel->insert($orderData, true);
                $session->setTempdata(['message' => 'Payment Successfull', 'type' => 'success'], NULL, 5);
                $emailHelper= new EmailHelper();
                $package= $packageModel->find($details['package_id']);
                $user = $userModel->find($details['user_id']);
                $emailHelper->send_payment_successful($user['first_name'], $user['last_name'], $user['email'], $package['price'], $package['name']);
                return redirect()->to(base_url().'/dashboard' );
            }catch (DatabaseException $e){
                return view('errors/404', ['message' => $e->getMessage(), 'code' => '404']);
            }
        }


    }

    public function cancel(){
        return view('errors/404', ['message' => "Sorry, the request was successful.<br>But the payment request was cancled.", 'code' => '201']);
    }

    public function return(){
        return view('errors/404', ['message' => "Sorry, the request was successful.<br>But the payment request was cancled and returned.", 'code' => '201']);

    }

}

<?php


namespace App\Controllers;


use App\Helpers\EmailHelper;
use App\Models\OrderModel;
use App\Models\PacakageModel;
use App\Models\UploadModel;
use App\Models\UserModel;
use CodeIgniter\Database\Exceptions\DatabaseException;
use CodeIgniter\Session\Session;
use function PHPUnit\Framework\throwException;

class Dashboard extends BaseController
{
    private $allowed_files = ['pdf', 'jpeg', 'jpg', 'png', 'docx'];
    public function index(){
        $session = session();
        if(!isset($session->get('isLoggedIn')['user'])){
            return view('errors/404', ['message' => "Previous user was logged out.<br>Please log in again.", 'code' => '400']);
        }
        $user = $session->get('isLoggedIn')['user'];
        $db = db_connect();
        $order = new OrderModel();
        $student_orders = $order->getUserTransactions($user['id']);
        $data = [
            'transactions' => $student_orders
        ];
        return view("dashboard/student/student-transactions", $data);
    }
    public function student_upload($id){
        $method = $this->request->getMethod();
        $order = new OrderModel();
        $upload = new UploadModel();
        $package = new PacakageModel();
        $order = $order->where('order.id', $id);
        if($method == "post"){
            $session = session();
            $user = $session->get('isLoggedIn')['user'];
            if(!isset($user)){
                return show_404();
            }
            $uid = $user['id'];
            $file = $this->request->getFile('document-upload');
            $notes = $this->request->getPost('document_notes');
            if(in_array($file->getExtension(), $this->allowed_files)){
                $path = $file->store();

                try {
                    $upload->insert(['path' => $path,
                                     'mime' => $file->getClientMimeType(),
                                     'name' => $file->getBasename(),
                                     'user_id'=> $uid,
                                     'notes' => $notes,
                                     'order_id' => $id]);
                    $order->increment('usage_count');
                    $session->setFlashdata(['message' => 'Document Upload Successful', 'type' => 'success']);
                } catch (DatabaseException $e) {
                    $session->setFlashdata(['message' => $e->getMessage(), 'type' => 'error']);
                    return redirect()->back();
                }
                try {
                    $email = new EmailHelper();
                    $res = $email->send_mail_to_admin($user['first_name'].' '.$user['first_name'], base_url('/upload/get/'.$path));
                }catch (\Error $e){
                    $session->setFlashdata(['message' => 'Email Sent Failed. <br/>'.$e->getMessage(), 'type' => 'error']);

                }
                return redirect()->back();
            }else{
                $session->setFlashdata(['message' => 'Provided file type is not supported.', 'type' => 'error']);
            }
        }
        $usage = $order->join('package', 'order.package_id=package.id')
            ->select('order.usage_count as order_count, package.usage_count as package_count')->first();
        $uploads = $upload->where('order_id', $id)
                         ->where('uploaded_by', 'admin')
                         ->join('order','order.id=order_id')
                         ->join('package','package.id = order.package_id')
                         ->select('package.name as package_name, path, upload.name as file_name,upload.notes as upload_notes, order.usage_count as usage_count, package.usage_count as package_usage_count, upload.created_at as created_at')
                         ->orderBy('created_at','desc')
                         ->findAll($usage['package_count']);

        $can_upload = $usage['order_count'] < $usage['package_count'];
        $data = [
            "order_id" => $id,
            "uploads" => $uploads,
            "can_upload" => $can_upload,
        ];
        return view("dashboard/student/student-upload", $data);
    }
}
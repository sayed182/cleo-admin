<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Helpers\EmailHelper;
use App\Models\AdminModel;
use App\Models\OrderModel;
use App\Models\PacakageModel;
use App\Models\UploadModel;
use App\Models\UserModel;
use CodeIgniter\Database\Exceptions\DatabaseException;

class Admin extends BaseController
{
    private $allowed_files = ['pdf', 'jpeg', 'jpg', 'png', 'docx'];
	public function index()
	{
	    $session = session();
        if(!isset($session->get('isLoggedIn')['is_admin'])){
            return view('errors/404', ['message' => "Page not available for unauthenticated users.", 'code' => '500']);
        }
        $db = db_connect();
        $users = $db
            ->query("Select user.id,first_name,last_name,user.email, upload.created_at as last_uploaded_on, order.created_at as last_ordered_on,
                        ( Select Count(*) from `order` where `order`.user_id = user.id)
                            as service_count from user
                            left join upload on upload.user_id = user.id
                            left join `order` on `order`.user_id = user.id
                            group by user.email
                            ")
            ->getResult('array');
        $db->close();
        if($this->request->getGet('service-count')){
            if($this->request->getGet('service-count') == 'no'){
                $users = array_filter($users, function ($var){
                    if($var['service_count'] == 0){
                        return $var;
                    }
                }, ARRAY_FILTER_USE_BOTH);
            }
            if($this->request->getGet('service-count') == 'yes'){
                $users = array_filter($users, function ($var){
                    if($var['service_count'] > 0){
                        return $var;
                    }
                }, ARRAY_FILTER_USE_BOTH);
            }

        }
        $data = [
            'users' => $users
        ];

		return view('dashboard/admin/admin-dashboard', $data);
	}
    public function services($id)
    {
        $order = new OrderModel();
        $orders = $order
                    ->where('order.user_id = ',$id)
                    ->join('package', 'order.package_id=package.id')
                    ->join('upload', 'upload.order_id = order.id', 'left')
                    ->select(['order.id',
                                'order.transaction_id',
                                'order.amount',
                                'package.name as package_name',
                                'order.notes',
                                'order.created_at',
                                'upload.path as file_path',
                                'upload.created_at  as ucat'])
                    ->orderBy('ucat', 'desc')
                    ->groupBy('order.id')
                    ->findAll();
        $data = ["orders" => $orders];
        return view('dashboard/admin/admin-services', $data);
    }
    public function upload($id){
        $method = $this->request->getMethod();
        $order = new OrderModel();
        $upload = new UploadModel();
        $package = new PacakageModel();
        $order = $order->where('order.id', $id);

        if($method == "post"){
            $session = session();
            if(!$session->get('isLoggedIn')){
                return view('errors/404', ['message' => "Previous user Session Expired Please login Again", 'code' => '500', 'title'=> "Session Expired"]);
            }
            $user = $session->get('isLoggedIn')['user'];
            if(!isset($user)){
                return show_404();
            }
            $uid = $order->first()['user_id'];
            $user = new UserModel();
            $user_email = $user->find($uid)['email'];
            $file = $this->request->getFile('document-upload');
            $notes = $this->request->getPost('document_notes');
            if(in_array($file->getExtension(), $this->allowed_files)){
                $path = $file->store();
                try {
                    $upload->insert(['path' => $path,
                        'mime' => $file->getClientMimeType(),
                        'name' => $file->getBasename(),
                        'user_id'=> $uid,
                        'order_id'=> $id,
                        'notes' => $notes,
                        'uploaded_by' => 'admin']);
                    $session->setFlashdata(['message' => 'Document Upload Successful', 'type' => 'success']);
                } catch (DatabaseException $e) {
                    $session->setFlashdata(['message' => $e->getMessage(), 'type' => 'error']);
                    return redirect()->back();
                }
                try {
                    $email = new EmailHelper();
                    $result = $email->send_mail($user_email,"Admin", base_url('/upload/get/'.$path));

                }catch (\Error $e){
                    $session->setFlashdata(['message' => 'Email Sent Failed. <br/>'.$e->getMessage(), 'type' => 'error']);
                }
                return redirect()->back();
            }else{
                $session->setFlashdata(['message' => 'Uploaded document type is not supported', 'type' => 'error']);
            }
        }

        $usage = $order->join('package', 'order.package_id=package.id')
            ->select('order.usage_count as order_count, package.usage_count as package_count')->first();
        $uploads = $upload->where('order_id', $id)
            ->where('uploaded_by', 'user')
            ->join('order','order.id=order_id')
            ->join('package','package.id = order.package_id')
            ->select('package.name as package_name, path, upload.name as file_name,upload.notes as upload_notes, order.usage_count as usage_count, package.usage_count as package_usage_count, upload.created_at as created_at')
            ->orderBy('created_at','desc')
            ->findAll($usage['package_count']);
        $reviewed = $upload->where('order_id', $id)
            ->where('uploaded_by', 'admin')
            ->join('order','order.id=order_id')
            ->join('package','package.id = order.package_id')
            ->select('package.name as package_name, path, upload.name as file_name,upload.notes as upload_notes, order.usage_count as usage_count, package.usage_count as package_usage_count, upload.created_at as created_at')
            ->orderBy('created_at','desc')
            ->findAll($usage['package_count']);
        $uid = $order->first()['user_id'];
        $can_upload = $usage['order_count'] < $usage['package_count'];
        $data = [
            "order_id" => $id,
            "user_id"  => $uid,
            "uploads" => $uploads,
            "reviewed" => $reviewed,
            "can_upload" => $can_upload,
        ];
        return view("dashboard/admin/admin-upload", $data);
    }
    public function login(){
        $method = $this->request->getMethod();
        if($method == 'post'){
            $email = $this->request->getPost('login-email');
            $password = $this->request->getPost('login-password');
            $userModel = new AdminModel();
            $user = $userModel->where('email', $email)->first();
            if(!isset($user)){
                return view('errors/404', ['message' => "No user found with the email id provided", 'code' => '500']);
            }
            if($user['password'] == md5($password)){
                $session = session();
                unset($user['password']);
                $session->set('isLoggedIn', [
                    'is_admin' => true,
                    'user' => $user,
                ]);
                return redirect()->to('/admin');
            }else{
                return view('errors/404', ['message' => "Provided password is in correct.<br>Please check your password again.", 'code' => '500']);
            }
        }
        return view('errors/404', ['message' => "Method not allowed.", 'code' => '500']);
    }
    public function logout(){
        $method = $this->request->getMethod();
        if($method == 'get'){
            $session = session();
            $session->destroy();
            return redirect()->to('/');
        }
    }
    public function package(){
	    $session = session();
	    if(isset($session->get('isLoggedIn')['user'])){
            $user = $session->get('isLoggedIn')['user'];
            if(isset($user['rights']) && $user['rights']!="WRITE"){
                return view('errors/404', ['message' => "You do not have permission to access this route", 'code' => '500']);
            }
	    }else{
            return view('errors/404', ['message' => "Please login with your admin credentials to view this page", 'code' => '500']);
        }
        $method = $this->request->getMethod();
        $packageModel = new PacakageModel();
        if($method == 'post'){
//            dd($this->request->getPost('package-atts'));

            $lastPackage = $packageModel->orderBy('created_at', 'desc')->first();
            $pid = substr($lastPackage['pid'], 1);
            if(!$this->request->getGet('pid')){
                $data['pid'] = 'p'.++$pid;
            }
            $data = [
                'id' => $this->request->getPost('package-id'),
                'name' => $this->request->getPost('package-name'),
                'price'=> $this->request->getPost('package-price'),
                'usage_count' => $this->request->getPost('package-usage'),
                'color' => $this->request->getPost('package-color'),
                'details' => $this->request->getPost('package-atts'),
            ];
            $saveid = $packageModel->save($data);
            if($saveid){
                $session = session();
                $session->setFlashdata(['message' => 'Successfully ran query', 'type' => 'success']);
                return redirect()->back();
            }else{
                $session = session();
                $session->setFlashdata(['message' => 'Failed running query', 'type' => 'error']);
                return redirect()->back();
            }
        }
        if($this->request->getGet('pid')){
            $package = $packageModel->find($this->request->getGet('pid'));
            return view('dashboard/admin/admin-add-package', ["package"=>$package]);
        }
	    return view('dashboard/admin/admin-add-package');
    }



//
//    private function modifyUpload(){
//	    $ordersModel = new OrderModel();
//
//	    $uploadModel = new UploadModel();
//	    $uploads = $uploadModel->findAll();
//	    foreach ($uploads as $upload){
//            print_r(explode('.',$upload['path'])[1]);
//            echo '<br>';
//            if(explode('.',$upload['path'])[1] == "pdf"){
//                $uploadModel->save(['id'=> $upload['id'], 'mime' => "application/pdf"]);
//            }
//
//        }
//        $newuploads = $uploadModel->findAll();
//	    dd($newuploads);
//    }
//    private function modifyUser(){
//        $userModel = new UserModel();
//        $users = $userModel->findAll();
//        foreach ($users as $user){
//            $userModel->save(['id'=>$user['id'], 'password'=> md5($user['password'])]);
//        }
//        $newUser = $userModel->findAll();
//        dd($newUser);
//    }
//    private function modifyOrder(){
//	    $orderModel = new OrderModel();
//	    $orders = $orderModel->findAll();
//	    foreach ($orders as $order){
//	        $id = substr($order['package_id'],1);
//	        $orderModel->save(['id'=> $order['id'], 'package_id'=>$id]);
//        }
//
//    }
//    private function decreaseUploadLimit(){
//        $orderModel = new OrderModel();
//        $packageModel = new PacakageModel();
//        $orders = $orderModel->findAll();
//        foreach ($orders as $order){
//            $package = $packageModel->find($order['package_id']);
//
//            if($order['usage_count'] > $package['usage_count']){
//                $usage_count = $package['usage_count'];
//                echo $usage_count. '<br>';
//                $orderModel->save(['id'=> $order['id'], 'usage_count'=>$usage_count]);
//            }
//
//        }
//    }

    public function testMail(){
	    try{
            $email = new EmailHelper();
            $mail = $email->send_mail("sayed182@gmail.com","Admin", base_url('/upload/get/20210718/newfile.pdf'));
            print_r($mail);
        }catch( \Exception $e){
	        print_r($e);
        }



    }

}

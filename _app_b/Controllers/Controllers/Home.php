<?php

namespace App\Controllers;

use App\Helpers\EmailHelper;
use App\Helpers\PaymentHelper;
use App\Models\AddressModel;
use App\Models\OrderModel;
use App\Models\PacakageModel;
use App\Models\UserModel;
use Cassandra\Date;
use CodeIgniter\Session\Session;
use Config\Database;

class Home extends BaseController
{
	public function index()
	{
        helper('html');
        $packageModel = new PacakageModel();
        $packages = $packageModel->findAll();
		return view('home/index', ['packages' => $packages]);
	}
	public function register($id)
    {
        $session = session();
        $packageModel = new PacakageModel();
        $packageData = $packageModel->find($id);
        $data = ["id"=>$id, "package" => $packageData];
        $method = $this->request->getMethod();
        if($method == "post"){

            $userModel = new UserModel();
            $addressModel = new AddressModel();

            $price = $this->request->getPost('package_price');
            $package_id = $this->request->getPost('package_id');
            $package_name= $this->request->getPost('package_name');

            if(isset($session->get('isLoggedIn')['user'])){
                $user = $session->get('isLoggedIn')['user'];
                $session->set('transaction_detail', [
                    'user_id' => $user['id'],
                    'package_id' => $id,
                ]);
                $this->proceedPayment($price);
            }else {

                $first_name = $this->request->getPost('first_name');
                $middle_name = $this->request->getPost('middle_name');
                $last_name = $this->request->getPost('last_name');
                $address = $this->request->getPost('address');
                $state = $this->request->getPost('state');
                $zip = $this->request->getPost('zip');
                $city = $this->request->getPost('city');
                $email = $this->request->getPost('email');
                $password = $this->request->getPost('password');
                $mobile = $this->request->getPost('mobile');
                $user = $userModel->where('email', $email)->first();
                if (!$user) {
                    $userData = [
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'middle_name' => $middle_name,
                        'email' => $email,
                        'mobile' => $mobile,
                        'password' => md5($password),
                    ];
                    $user_id = $userModel->insert($userData, true);
                    $user = $userModel->find($user_id);
                }
                if ($user['password'] != md5($password)) {
                    return view('errors/404', ['message' => "User with this email ID is already registered.<br>But the provided credentials are not correct.<br> Please try again with correct password or <a href='".base_url('/home/forgotpassword')."'>CLICK HERE</a> to reset your password.", 'code' => '500', "title" => "Incorrect Credentials"]);
                }
                $addressData = [
                    'address' => $address,
                    'state_name' => $state,
                    'city_names' => $city,
                    'zip_code' => $zip,
                ];


                $addressData['user_id'] = $user['id'];
                $address_id = $addressModel->insert($addressData, true);
                $result = $user["id"] > 0 && $address_id > 0;
                $data['result'] = $result;
                if ($result) {

                    $session->set('isLoggedIn', [
                        'is_admin' => false,
                        'user' => $user,
                    ]);
                    $session->set('transaction_detail', [
                        'user_id' => $user['id'],
                        'package_id' => $id,
                    ]);
                    $emailHelper = new EmailHelper();
                    $emailHelper->send_successfull_registered($user['first_name'], $user['email'], $user['mobile']);
                }

                $this->proceedPayment($price);
            }

        }

        return view('home/register', $data);
    }
    private function proceedPayment($price){
	    $session = session();
        $gateway = new PaymentHelper();
        $response = $gateway->purchase(
            array(
                'cancelUrl'=>base_url().'/payment/cancel',
                'returnUrl'=>base_url().'/payment/success',
                'amount' =>  number_format($price, 2, '.', ''),
                'currency' => 'USD',
            )
        );
        if($response->isRedirect()){
            $response->redirect();
        }else{
            echo $response->getMessage();
            $session->setFlashdata(['message' => $response->getMessage() , 'type' => 'error']);
            return redirect()->back();
        }
    }

    public function login(){
        $method = $this->request->getMethod();
        if($method == 'post'){
            $email = $this->request->getPost('login-email');
            $password = $this->request->getPost('login-password');
            $userModel = new UserModel();
            $user = $userModel->where('email', $email)->first();
            if(!isset($user)){
                return view('errors/404', ['message' => "No user found with the email id provided", 'code' => '500', 'title' => "No user found."]);
            }
            if($user['password'] == md5($password)){
                $session = session();
                unset($user['password']);
                $session->set('isLoggedIn', [
                    'is_admin' => false,
                    'user' => $user,
                ]);
                return redirect()->to('/dashboard');
            }else{
                return view('errors/404', ['message' => "Provided password is incorrect.<br>Please check your password again.", 'code' => '500']);
            }
        }
        return view('errors/404', ['message' => "Method not allowed.", 'code' => '500']);
    }
    public function logout(){
        $method = $this->request->getMethod();
        if($method == 'get'){
            $session = session();
            $session->destroy();
            return redirect()->to('/');
        }
    }
    public function forgetpassword(){
        $session = session();
	    $method = $this->request->getMethod();
	    if($method == "post"){
	        $user_email = $this->request->getPost('email');
//	        dd($this->request->getPost('email'));
	        $userModel = new UserModel();
	        $user = $userModel->where('email', $user_email)->first();
	        if($user){
	            $date = date('Y-m-d H:i:s');
	            $token = md5($date);
                $db      = \Config\Database::connect();
                $builder = $db->table('password_reset');
                $builder->insert(['user_id'=> $user['id'], 'token'=> $token]);
                $db->close();
                try{
                    $email = new EmailHelper();
                    $email->send_reset_mail($user_email, $token);
                    $session->setFlashdata(['message' => 'Reset email successfully sent', 'type' => 'success']);
                }catch (\Error $e){
                    $session->setFlashdata(['message' => 'Email Sent Failed. <br/>'.$e->getMessage(), 'type' => 'error']);

                }

            }
	        else{

                $session->setFlashdata(['message' => 'Email ID entered is not registered.', 'type' => 'error']);
            }

        }
        return view('/home/forgetpassword');
    }
    public function setpassword(){
        $token = $this->request->getGetPost('token');
//        print_r($token);
        $session = session();
        $method = $this->request->getMethod();
        if($token){
            $db      = \Config\Database::connect();
            $builder = $db->table('password_reset');
            $query = $builder->where('token', $token)->orderBy('created_at', 'desc')->get();
            $result = $query->getLastRow('array');
            $date = new \DateTime($result['created_at']);
            $current_time = new \DateTime();
            $diff = $date->diff($current_time);
            if($diff->d !=0 && $diff->h > 2 ){
                $db->close();
                return view('errors/404', ['message' => "Provided token has expired please try again.", 'code' => '500']);
            }
            if($result && $method == "post"){
                $password = $this->request->getPost('password');
                $cpassword = $this->request->getPost('cpassword');
                if($password != $cpassword){
                    $session->setFlashdata(['message' => 'Your entered password and confirm password are not same', 'type' => 'error']);
                }else {
                    $userModel = new UserModel();
                    $user = $userModel->find($result['user_id']);
                    $userModel->save([
                        'id' => $result['user_id'],
                        'password' => md5($password),
                    ]);
                    $session->setFlashdata(['message' => 'Your Password has been successfully changed.', 'type' => 'Success']);
                    $db->close();
                    return redirect()->to('/home');
                }
            }

        }
        return View("home/resetpassword",['token'=>$token]);
    }
    public function clearSession(){
	    if(isset($_SESSION['message'])){
	        unset($_SESSION['message']);
	        unset($_SESSION['type']);
        }
    }
    private function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

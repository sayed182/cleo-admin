<?php
namespace App\Helpers;

use Omnipay\Omnipay;
class PaymentHelper
{
    /**
     * @return mixed
     */
    public function gateway()
    {
        $gateway = Omnipay::create('PayPal_Rest');
        // ------- Clas Production Api Credentials -----//
        $gateway->setClientId("AYakJG_SjABTxXYylpfQYmTq4yEGsZ1hgXCt9eYnxhqhJt4td2gzdjWUiek8MIEOt9FcVn2sKhEXep8T");
        $gateway->setSecret("EF8K8j3iQukcyIdD-rGs2HLOdzKQMPJRsbre2hKoe9qwNDGo3n8dFleoGbxMah9Ic-vd0BeUNRWgF85c");
        $gateway->setTestMode(false);
        
        // ------- Sayed Dev Api Credentials -----//
        // $gateway->setClientId("ATpVXuLICPnh4JnhCYvzZ6wbfKweHZXgPWzK7d7QyUXaG0n7Y2bWsqVx-2DGzHfajQ4BHTT-G23FknRL");
        // $gateway->setSecret("ELSBl_E_ctx16012YXhHIRtdoRnwJJ1oX4xz8ALklE9r7y0U7bI3gXqH30BoB81Fe1KprOStcpmzASyJ");
        // $gateway->setTestMode(true);
        return $gateway;
    }
    /**
     * @param array $parameters
     * @return mixed
     */
    public function purchase(array $parameters)
    {
        $response = $this->gateway()
            ->purchase($parameters)
            ->send();
        return $response;
    }
    /**
     * @param array $parameters
     */
    public function complete(array $parameters)
    {
        $response = $this->gateway()
            ->completePurchase($parameters)
            ->send();
        return $response;
    }
    /**
     * @param $amount
     */
    public function formatAmount($amount)
    {
        return number_format($amount, 2, '.', '');
    }
    /**
     * @param $order
     */
    public function route($name, $params)
    {
        return $name; // ya change hua hai
    }
}